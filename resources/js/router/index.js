import {createWebHistory, createRouter} from "vue-router";

import Register from '../pages/Register';
import Login from '../pages/Login';
import Pizzas from "../pages/pizzas/Pizzas";
import PizzasCreate from "../pages/pizzas/PizzasCreate";
import PizzasEdit from "../pages/pizzas/PizzasEdit";
import Ingredients from "../pages/ingredients/Ingredients";
import IngredientsCreate from "../pages/ingredients/IngredientsCreate";
import IngredientsEdit from "../pages/ingredients/IngredientsEdit";

export const routes = [
	{
		name: '/',
		path: '/',
		component: Login
	},
	{
		name: 'login',
		path: '/login',
		component: Login
	},
	{
		name: 'register',
		path: '/register',
		component: Register
	},

	{
		name: 'pizzas',
		path: '/pizzas',
		component: Pizzas
	},
	{
		name: 'pizzas.create',
		path: '/pizzas/create',
		component: PizzasCreate
	},
	{
		name: 'pizzas.edit.id',
		path: '/pizzas/edit/:id',
		component: PizzasEdit
	},
	{
		name: 'ingredients',
		path: '/ingredients',
		component: Ingredients
	},
	{
		name: 'ingredients.create',
		path: '/ingredients/create',
		component: IngredientsCreate
	},
	{
		name: 'ingredients.edit.id',
		path: '/ingredients/edit/:id',
		component: IngredientsEdit
	},
];

const router = createRouter({
	history: createWebHistory(),
	routes: routes,
});

export default router;
