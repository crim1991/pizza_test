import {createApp} from 'vue'
import SvgIcon from "vue3-icon";

require('./bootstrap')
import App from './App.vue'
import axios from 'axios'
import router from './router'
// import Select2Component
import Select2 from 'vue3-select2-component';

const app = createApp(App)
app.component("svg-icon", SvgIcon);
app.component('MySelect', Select2);

app.config.globalProperties.$axios = axios;
app.use(router)
app.mount('#app')
