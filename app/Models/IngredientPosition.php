<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class IngredientPosition extends Model
{
    use HasFactory;

    protected $table = 'ingredient_position';

    protected $fillable = [
        'ingredient_id',
        'user_id',
        'position',
    ];

    public static function updateCurrentElement($request)
    {
        IngredientPosition::where('user_id', Auth::user()->id)
            ->where('ingredient_id', $request->post('id'))
            ->update(['position' => $request->post('newIndex')]);
    }
}
