<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pizza extends Model
{
    use HasFactory;

    protected $table = 'pizza';

    protected $fillable = [
        'name',
        'price',
        'user_id',
    ];

    public static function create($data)
    {
        $user = Auth::user();
        $pizza = new Pizza();
        $pizza->name = $data['name'];
        $pizza->user_id = $user->id;
        $pizza->price = $data['price'];
        $pizza->save();

        return $pizza->id;
    }

    public static function get()
    {
        return Pizza::select('pizza.id', 'pizza.price', 'pizza.name',
            DB::raw('(
                SELECT GROUP_CONCAT(ingredients.name SEPARATOR ", ")
                    FROM ingredients
                LEFT JOIN pizza_ingredients ON pizza_ingredients.ingredient_id = ingredients.id
                WHERE pizza_ingredients.ingredient_id = ingredients.id
                ) as ingredients'
            )
        )
            ->where('pizza.user_id', Auth::user()->id)
            ->groupBy('pizza.id')
            ->get();
    }

    public static function getById($id)
    {
        return Pizza::select('pizza.id', 'pizza.name',
            DB::raw('(
                SELECT GROUP_CONCAT(CONCAT(ingredients.id, " " , ingredients.name) SEPARATOR ",")
                    FROM ingredients
                LEFT JOIN pizza_ingredients ON pizza_ingredients.ingredient_id = ingredients.id
                WHERE pizza_ingredients.ingredient_id = ingredients.id
                ) as ingredients'
            )
        )
            ->where('pizza.user_id', Auth::user()->id)
            ->where('pizza.id', $id)
            ->groupBy('pizza.id')
            ->get();
    }
}
