<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PizzaIngredients extends Model
{
    use HasFactory;

    protected $table = 'pizza_ingredients';

    protected $fillable = [
        'pizza_id',
        'ingredient_id',
    ];

    public static function makeNew($id, $pizza_id)
    {
        PizzaIngredients::create([
            'ingredient_id' => $id,
            'pizza_id' => $pizza_id
        ]);
    }
}
