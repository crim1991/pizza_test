<?php

namespace App\Logic;

use App\Models\IngredientPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IngredientPositionManager
{
    private $id;
    private $newIndex;
    private $oldIndex;
    private $oldElementID;

    public function __construct(Request $request)
    {
        $this->id = $request->post('id');
        $this->newIndex = $request->post('newIndex');
        $this->oldIndex = $request->post('oldIndex');
    }

    protected function changePositions() {
        $this->incrementPosition();
        $this->decrementPosition();
    }

    protected function incrementPosition()
    {
        $freeIngredients = IngredientPosition::where('user_id', Auth::user()->id)
            ->where('position', '>=', $this->newIndex)
            ->where('ingredient_id', '!=', $this->id)
            ->get();

        if ($freeIngredients->count() !== 0) {
            foreach ($freeIngredients as $ingredient) {
                IngredientPosition::where('id', $ingredient->id)
                    ->update(['position' => $ingredient->position + 1]);
            }
        }
    }

    protected function decrementPosition()
    {
        $freeIngredients = IngredientPosition::where('user_id', Auth::user()->id)
            ->where('position', '<=', $this->newIndex)
            ->where('ingredient_id', '!=', $this->id)
            ->get();

        foreach ($freeIngredients as $ingredient) {
            IngredientPosition::where('id', $ingredient->id)
                ->update(['position' => $ingredient->position - 1]);
        }
    }

    public function updatePosition()
    {
        $oldElement = IngredientPosition::where('user_id', Auth::user()->id)
            ->where('position', $this->newIndex)
            ->where('ingredient_id', '!=', $this->id)
            ->first();

        if (!is_null($oldElement)) {
            $this->oldElementID = $oldElement->id;
        }

        if (($this->newIndex - 1) > $this->oldIndex
            XOR ($this->oldIndex - 1) > $this->newIndex) {
            if (($this->newIndex - 1) > $this->oldIndex) {
                $this->decrementPosition();
            } elseif (($this->oldIndex - 1) > $this->newIndex) {
                $this->incrementPosition();
            }
        } else {
            IngredientPosition::where('user_id', Auth::user()->id)
                ->where('position', $this->newIndex)
                ->where('ingredient_id', '!=', $this->id)
                ->update(['position' => $this->oldIndex]);
        }
    }
}
