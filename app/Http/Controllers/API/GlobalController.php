<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Ingredients;

class GlobalController extends Controller
{
    public function getIngredients()
    {
        $data = Ingredients::select('ingredients.id', 'name as text')
            ->get()->toArray();


        return response()->json($data);
    }
}
