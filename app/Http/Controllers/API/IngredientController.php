<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\IngredientRequest;
use App\Logic\IngredientPositionManager;
use App\Models\IngredientPosition;
use App\Models\Ingredients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Ingredients::select('ingredients.id', 'name', 'cost')
            ->leftJoin('ingredient_position', 'ingredients.id', 'ingredient_position.ingredient_id')
            ->orderBy('ingredient_position.position', 'ASC')
            ->get()->toArray();
        return response()->json($data);
    }

    public function getIngredients()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientRequest $request)
    {
        $data['name'] = $request->post('name');
        $data['cost'] = bcmul($request->post('cost'), 100);

        try {
            $ingredient = Ingredients::create($data);
            $position = IngredientPosition::select('id')->get()->count();
            if (is_null($position)) { $position = 0; }
            IngredientPosition::create([
                'ingredient_id' => $ingredient->id,
                'user_id' => Auth::user()->id,
                'position' => $position
            ]);
            $success = true;
            $message = 'Ingredient created successfully';
        } catch (QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        $response = [
            'success' => $success,
            'message' => $message
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingredient = Ingredients::where('id', $id)->first();
        return response()->json(['ingredient' => $ingredient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['name'] = $request->post('name');
        $data['cost'] = bcmul($request->post('cost'), 100);
        $result = Ingredients::where('id', $id)->update($data);

        if ($result) {
            $success = true;
            $message = 'Ingredient updated';
        } else {
            $success = false;
            $message = 'Please contact administrator';
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }

    public function updatePosition(Request $request)
    {
        $ingredientPosition = new IngredientPositionManager($request);
        $ingredientPosition->updatePosition();
        IngredientPosition::updateCurrentElement($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove = Ingredients::destroy($id);

        if ($remove) {
            $success = true;
            $message = 'Ingredient removed';
        } else {
            $success = false;
            $message = 'Please contact administrator';
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);

    }
}
