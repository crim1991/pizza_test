<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PizzaRequest;
use App\Http\Requests\PizzaUpdateRequest;
use App\Models\Ingredients;
use App\Models\Pizza;
use App\Models\PizzaIngredients;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Pizza::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PizzaRequest $request)
    {
        //get ingredients by id
        $ingredients = $request->post('ingredients');
        $data['name'] = $request->post('name');
        $data['price'] = Ingredients::findMany($ingredients)->sum('cost');

        try {
            $pizza_id = Pizza::create($data);
            foreach ($ingredients as $id) {
                PizzaIngredients::makeNew($id, $pizza_id);
            }
            $success = true;
            $message = 'Pizza created successfully';
        } catch (QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(Pizza::getById($id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PizzaUpdateRequest $request, $id)
    {
        try {
            Pizza::where('id', $id)
                ->update([
                    'name' => $request->post('name')
                ]);
            PizzaIngredients::where('pizza_id', $id)->delete();
            $ingredients = $request->post('ingredients');
            foreach ($ingredients as $item) {
                PizzaIngredients::create([
                    'pizza_id' => $id,
                    'ingredient_id' => $item
                ]);
            }
            $success = true;
            $message = 'Pizza updated successfully';
        } catch(QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remove = Pizza::destroy($id);

        if ($remove) {
            $success = true;
            $message = 'Pizza removed';
        } else {
            $success = false;
            $message = 'Please contact administrator';
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }
}
